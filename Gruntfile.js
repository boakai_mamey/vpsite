/*global module:false*/
module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    // Metadata.
    pkg: grunt.file.readJSON('package.json'),
    banner: '/*! <%= pkg.title || pkg.name %> - v<%= pkg.version %> - ' +
      '<%= grunt.template.today("yyyy-mm-dd") %>\n' +
      '<%= pkg.homepage ? "* " + pkg.homepage + "\\n" : "" %>' +
      '* Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author.name %>;' +
      ' Licensed <%= _.pluck(pkg.licenses, "type").join(", ") %> */\n',
    // Task configuration.
    lessFiles: [{
      expand: true,
      cwd: 'less/',
      src: '**/*.less',
      dest: 'css/',
      ext: '.css'
    }],
    jsFiles: [{
      cwd: 'js/',
      src: '**/*.js',
      dest: 'min/',
      ext: '.min.js'
    }],
    clean: {
      css: {
        src: ['css/all.css'],
        filter: 'isFile',
        options: {
            force: true
          },
      }
    },
    concat: {
      options: {
        separator: ';',
        cleancss: true
      },
      js: {
        src: 'js/*.js',
        dest: 'js-min/'
      },
      css: {
        src: 'css/*.css',
        dest: 'css/all.css',
      }
    },
    less: {
     options: {
          cleancss: true,
          report: 'min'
        },
      dist: { 
        files: '<%= lessFiles %>',
        options: {
          cleancss: true,
          report: 'min'
        }
      }
    },
    uglify: {
        // options: {
        //   report: 'min'
        // },
        // dist: {
        //   files: '<%= jsFiles %>',
        // },
        // options: {
        //   report: 'min'
        // }
        dist: {
        options: {
          preserveComments: 'some',
          report: 'min'
        },
        
        files: [{
          expand: true,
            cwd: 'js',
            src: ['*.js'],
            dest: 'js-min/',
            ext: '.min.js'
        }]
      }
    },
    jshint: {
      options: {
        curly: true,
        eqeqeq: true,
        immed: true,
        latedef: true,
        newcap: true,
        noarg: true,
        sub: true,
        undef: true,
        unused: true,
        boss: true,
        eqnull: true,
        globals: {}
      },
      gruntfile: {
        src: 'Gruntfile.js'
      },
      lib_test: {
        src: ['lib/**/*.js', 'test/**/*.js']
      }
    },
    nodeunit: {
      files: ['test/**/*_test.js']
    },

    // uglify: {

    //   dist: {
    //     options: {
    //       preserveComments: 'some',
    //       report: 'min'
    //     },
        
    //     files: [{
    //       expand: true,
    //         cwd: 'js',
    //         src: ['**/*.js'],
    //         dest: 'dist/js/',
    //         ext: '.min.js'
    //     }]
    //   }

    // },
    watch: {
            styles: {
               options: {
                    spawn: false,
                    event: ["added", "deleted", "changed"]
                },
                files: [ "less/**/*.less"],
                tasks: [ "less:dist", "clean:css", "concat:css"]
            },
            scripts: {
              options: {
                    spawn: false,
                    event: ["added", "deleted", "changed"]
                },
                files: ["js/*.js"],
                tasks: ["uglify:dist"] 
            }
        }
  });

  // These plugins provide necessary tasks.
  grunt.loadNpmTasks('grunt-contrib-concat');
   grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-nodeunit');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-lesslint');
  

  // Default task.
  grunt.registerTask('default', ['watch']);

};
