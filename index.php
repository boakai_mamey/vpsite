<!doctype html>
<html lang="en">

<head>
<meta charset="utf-8">

<title></title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="bootstrap/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- Optional theme -->
<link rel="stylesheet" href="bootstrap/dist/css/bootstrap-theme.min.css">

<link rel="stylesheet" href="http://s.mlcdn.co/animate.css">

<link rel="stylesheet" type="text/css" href="css/all.css">
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<style>

</style>

<!--[if lt IE 8]>
<style>
/* For IE < 8 (trigger hasLayout) */
.clearfix {
    zoom:1;
}
</style>
<![endif]-->


</head>


<body>
<section id="page">

<header class="navbar navbar-static-top" role="banner"> 
	<div id="top-page-nav">
		<div class="container"> 
			<nav id="small-page-nav" class=""> 
				<ul class="nav navbar-nav"> 
					<li class="active"> <a href="#">Donate</a> </li> 
					<li class="active"> <a href="#">Volunteer</a> </li> 
					<li class="active"> <a href="#">Events</a> </li> 
				</ul> 
				<ul class="nav navbar-nav navbar-right"> 
					<li><a href="#"><i class="fa fa-facebook-official"></i></a></li> 
					<li><a href="#"><i class="fa fa-instagram"></i></a></li> 
					<li><a href="#"><i class="fa fa-twitter"></i></a></li> 
				</ul> 
			</nav> 
		</div> 
	</div>


	<div id="main-page-nav">
		<div class="container"> 
			<div class="navbar-header"> 
				<button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#page-nav" aria-controls="page-nav" aria-expanded="false"> 
					<span class="sr-only">Toggle navigation</span> 
					<span class="icon-bar"></span> 
					<span class="icon-bar"></span> 
					<span class="icon-bar"></span> 
				</button> 
				<a href="index.php" class="navbar-brand"></a> 
			</div> 
			<nav id="page-nav" class="collapse navbar-collapse"> 
				<ul class="nav navbar-nav"> 
					
				</ul> 
				<ul class="nav navbar-nav navbar-right mainNav"> 
					<li><a class="current" href="#">Home</a></li> 
					<li><a href="#">About Us</a></li> 
					<li><a href="#">Our Candidate</a></li>
					<li><a class="contact" href="#">Contact</a></li>
					<li>
						<li> <div id="sb">
						<form id="searchbox" action="http://www.google.com/search" method="get" onsubmit="Gsitesearch(this)">
						<input name="q" type="hidden">

						  <p class="s"><input name="qfront" id="search" type="search"> </p>

						</form>


					</div></li>
					
				</ul>


			</nav> 
		</div> 
	</div>
</header>


	<section>

<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
    <li data-target="#carousel-example-generic" data-slide-to="3"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">

    <div class="item active">
       <img src="images/smilingJB.jpg" alt="...">
       <div class="carousel-caption">
         
       </div>
     </div>
   <div class="item">
      <img src="images/onj1.jpg" alt="...">
      <div class="carousel-caption">
      </div>
    </div>
     <div class="item">
      <img src="images/gg2.jpg" alt="...">
      <div class="carousel-caption">
      </div>
    </div>

    <div class="item">
      <img src="images/josephbokai_slider77.jpg" alt="...">
      <div class="carousel-caption">
      </div>
    </div>


    
  </div>
 <!-- home overlay -->
 <div id="home-overlay">
 </div>



  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
  <div class="small-strip">
	  	<div class="container">
		  	<div class="row strip-row">
			  <div class="col-xs-4 col-md-4"><p><span><i class="fa fa-users"></i>Volunteer</span> <br> Get involved with the campaign</p></div>
			  <div class="col-xs-4 col-md-4"> <p><span><i class="fa fa-money"></i>Donate</span> <br> Contribute to the campign funds</p><br> <p></p></div>
			  <div class="col-xs-4 col-md-4"><p><span><i class="fa fa-truck"></i>Spread The Word</span> <br> Tell others about joning NAMBO</p></div>
			</div>
	  </div>
  </div>
 
</div>


	  <article >
	  	 
         <div class="mid-content">             
         <header class="welcomeh">
         	<div class="container">
         		<div class="bhcontent">
         			<h1>Welcome to NAMBO!</h1>
         			<p class="hp">The national movement to support Honorable Joseph N. Boakai, a man with vision for Liberia</p>
         		</div>
         		
         	</div>
         	<div class="arrow-down"></div>
         </header>             <!-- Fix here
-->         
			<div class="container ct1">
				<div class="row">                  
					<div class="col-sm-6 col-md-4">                      
						<div class="thumbnail"> 
							<img src="images/NebogroupSmall.jpg" alt="featured" data-holder-rendered="true" style="height: 200px; width: 100%; display: block;"> 
							<div class="caption">
								<h3><em>About NAMBO</em></h3> <p>National Movement to Support Joseph N. Boakai (NAMBO) believe in the advancement 
								of Liberia and that with a selfless leader like Joseph Boakai, Liberia will rise again.</p> <p><a href="#" class="btn btn-primary"
								role="button">Button</a> </p> 
							</div> 
						</div> 

					</div> 

                    <div class="col-sm-6 col-md-4">                                               <div
class="thumbnail">                              <img src="images/vpafricanSmall.jpg" alt="featured" data-holder-rendered="true"
style="height: 200px; width: 100%; display: block;">                              <div class="caption">
<h3> <em>Meet Hon. Boakai </em></h3> <p>After all, who is better suited to fly a plane when the pilot leaves  
the cock  pit besides the co-pilot? Ambassador Boakai, having lengthy years of experience is the one
for 2017.</p> <p><a href="#" class="btn
btn-primary"                                 role="button">Button</a> </p>                              </div>
</div>

					</div> 

					<div class="col-sm-6 col-md-4">                      
						<div class="thumbnail"> 
							<img src="images/platformPic.jpg" alt="featured" data-holder-rendered="true" style="height: 200px; width: 100%; display: block;"> 
							<div class="caption">
								<h3><em>The Platform</em></h3> <p>Economic development, quality education, infrastructure development, and excellent heath care
								are our top priorties. We will do this by utilizing our resources and foriegn relations.</p> <p><a href="#" class="btn btn-primary"
								role="button">Button</a> </p> 
							</div> 
						</div> 

					</div> 

				</div> <!-- row ends -->
			</div>

<section id="parallax1"><!-- general parallax1-->
					<div class="p-overlay">


					</div>
					<div class="pad"><q class='vpq'>We will rebuild this country with the help of all Liberians. <span>Love Liberia!</span> <span>Think Liberia!</span> <span>Live Liberia!</span></q>
					 </div>
		    	
			</section>



			<!-- <div class="container longBanner"> -->
			<div class="ct2 tes-pic">
				<div class="row"><!--  style="   height: 450px;" -->
					<div class="col-md-6 standInfo">
					 <h1>What We Stand for</h1>
					 <ul>
					 	<li><i class="fa fa-road fa-3x"></i>Infrastrature Development</li>
					 	<li><i class="fa fa-users fa-3x"></i>Peace &amp; Unity</li>
					 	<li><i class="fa fa-graduation-cap fa-3x"></i>Quality Education</li>
					 	<li><i class="fa fa-briefcase fa-3x"></i>Economic Oppertunity</li>
					 </ul>

					</div>
					<div class="col-md-6"> </div>
				</div>
			</div>

			
			</div> 
			<!--<div class="ct3">
				<div class="row twoColumnHigl">
					<div style="background-color: #eee; height: 300px;" class="col-md-6 "></div>
					<div style="background-color: #d4d4d4; height: 300px;" class="whyJB col-md-6">
						<div class="container">
							<div class="row">
								<h2>VOTE JOSEPH BOAKAI FOR</h2>
								<div class="col-md-12">Rapid Infrasture Development</div>
								<div class="col-md-12">Rapid Infrasture Development</div>
								<div class="col-md-12">Rapid Infrasture Development</div>
								<div class="col-md-12">Rapid Infrasture Development</div>
							</div>
						</div>
					</div> whyJB col-md-6 

				</div> twoColumnHigl 
			</div>-->

		</div> <!-- main content ends -->



	  </article>
	</section>

	<section>
	  <article > </article>
	  <article> </article>
	</section>

	<aside > </aside>

	<!-- <footer> footer content here</footer> -->

</section>

<section>
	<footer class="main" style="text-align:center;">
		<p class="cright"> NAMBO © 2016 All rights reserved. | <span>Designed by: <a href="http://bkdesignit.com" target="_blank">Boakai Mamey</a> &amp; <a href="http://fm.bkdesignit.com" target="_blank">Fabunde Mamey</a> <span></p>
	</footer>
</section>
<script src="js-min/jquery-1.12.0.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="js-min/script.min.js"></script>

<script src="js/jquery.appear.js" type="text/javascript"></script>
<script src="js/custom.js" type="text/javascript"></script>

<script type="text/javascript">
	
	/*
	* we will need to attach this to and event so that
	* this calculation happens when the window is getting 
	* smaller
	*/ 
	var scrHeight = $(window).height() - ($('.navbar').height());
	var bgHolder = $('div.carousel-inner');
	//$('#carousel-inner')
	bgHolder.css("height", scrHeight);

	
	$('#carousel-example-generic .item img').each(function(){
		var currentImg = $(this).attr("src");
		$(this).parent().css("background-image", "url(" + currentImg + ")");
		$(this).remove();
	});
	


	$(window).resize(function(){
	scrHeight = $(window).height() - $('.navbar').height();
	bgHolder.css("height", scrHeight);
	});

	//$(Document)
	
	//change img to background 

	/*$('#carousel-example-generic .item img').each(function(){
	// = $('div.item img')
	var theImg = $(this).attr('src');
	$(this).parent().css({"background": "url(" + theImg + ")"});
	//$(this).parent().css("height", scrHeight)

	$(this).remove();
	});

	$(window).resize(function(){
	scrHeight = $(window).height() - $('.navbar').height();
	$('#carousel-example-generic').css("height", scrHeight);
	});*/

</script>
</body>
</html>