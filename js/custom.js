
$(function () {
	"use strict";
    
    /*global jQuery */ 
	//Slider 
	$(document).ready(function(){
		
		$('.strip-row p').each(function(){
			$(this).appear(function(){
				//$(this).animate({opacity: 1}, 1000);
				$(this).addClass('animated fadeInUp');
			});
		});
	

		$('div.thumbnail').each(function(){
			$(this).appear(function(){

				//$(this).animate({opacity: 1}, 1000);
				$(this).addClass('animated zoomIn');
			});
		});

		$('#sb').appear(function(){
				//$(this).animate({opacity: 1}, 1000);
				$(this).addClass('animated rotateIn');
			});

		$('.bhcontent h1').appear(function(){
				//$(this).animate({opacity: 1}, 1000);
				$(this).addClass('animated rubberBand');
			});

// the others should be changed to eppear function if you dont need the anynomous function
		$('.standInfo > h1').appear(function(){
				$(this).addClass('animated zoomInDown');
			});

		$('.vpq').appear(function(){
				$(this).addClass('animated zoomIn');
			});
		


		$('.standInfo li').each(function() {
			$(this).appear(function() {
				$(this).addClass('animated slideInLeft');
				//$(this).animate({"left": "10px"}, 1000);

				//animated bounce			
			});
		});	



	});
	
}());
